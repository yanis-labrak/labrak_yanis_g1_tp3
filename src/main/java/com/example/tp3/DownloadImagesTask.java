package com.example.tp3;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.net.URL;

public class DownloadImagesTask extends AsyncTask<ImageView, Void, Bitmap> {

    ImageView imageView;
    Team team;

    public DownloadImagesTask(ImageView imageView, Team team) {
        this.imageView = imageView;
        this.team = team;
    }

    @Override
    protected Bitmap doInBackground(ImageView... imageViews) {

        Bitmap bmp = null;

        try {
            URL url = new URL(this.team.getTeamBadge());
            bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (Exception e) {
            Log.d("error_badge",e.toString());
        }

        return bmp;
    }

    @Override
    protected void onPostExecute(Bitmap result) {

        imageView.setImageBitmap(result);

    }
}