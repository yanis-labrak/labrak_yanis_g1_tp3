package com.example.tp3;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class NewTeamActivity extends AppCompatActivity {

    private EditText textTeam, textLeague;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_team);

        textTeam = (EditText) findViewById(R.id.editNewName);
        textLeague = (EditText) findViewById(R.id.editNewLeague);

        final Button but = (Button) findViewById(R.id.update);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String league = textLeague.getText().toString();

                String teamName = textTeam.getText().toString();

                if (teamName.isEmpty()) {

                    new AlertDialog.Builder(NewTeamActivity.this)
                    .setTitle("Sauvegarde impossible")
                    .setMessage("Le nom de l'équipe doit être non vide.")
                    .show();

                } else {

                    // Set uppercase
                    teamName = teamName.substring(0, 1).toUpperCase() + teamName.substring(1);

                    // Create the team
                    Team team = new Team(teamName,league);

                    Intent intent = new Intent();
                    intent.putExtra(Team.TAG, team);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
//        Intent intent = new Intent(NewTeamActivity.this,MainActivity.class);
//        startActivity(intent);
        super.onBackPressed();
    }
}
