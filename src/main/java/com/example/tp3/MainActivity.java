package com.example.tp3;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    RecyclerView listTeam;
    public static SwipeRefreshLayout swipe;

    SportDbHelper dbHelper;
    public static TeamRecyclerAdapter adapter;

    public static SportDbHelper DB;

    public static int nbrThreadsRunningRefresh = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);

        MainActivity.DB = new SportDbHelper(this);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, NewTeamActivity.class);
                MainActivity.this.startActivityForResult(myIntent,1);
            }
        });

        dbHelper = new SportDbHelper(this);
//        dbHelper.populate();

        this.linkFields();
        this.initFields();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK) {

            Team team = (Team) data.getParcelableExtra(Team.TAG);

            if (team != null) {

                MainActivity.DB.addTeam(team);

                // Update the listview
                MainActivity.adapter.notifyDataSetChanged();

                Toast.makeText(this, "Team saved !", Toast.LENGTH_SHORT);

            } else {
                Toast.makeText(this, "The team is strange !", Toast.LENGTH_SHORT);
            }

        } else {
            Toast.makeText(this, "Something happened !", Toast.LENGTH_SHORT);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {

        for (Team team : MainActivity.DB.getAllTeams()) {

            RefreshTeamContent runningTask = new RefreshTeamContent();
            runningTask.execute(team);

            MainActivity.nbrThreadsRunningRefresh++;
        }

    }

    /**
     * Link all the fields to the controller
     */
    private void linkFields() {
        this.listTeam = findViewById(R.id.listTeam);
        swipe = findViewById(R.id.swipe);
    }

    /**
     * Initialize all the fields
     */
    private void initFields() {

        this.initList();

        // Swipe to refresh
        swipe.setOnRefreshListener(this);

        // Remove with slide
        new ItemTouchHelper(removeCallback).attachToRecyclerView(listTeam);
    }

    /**
     * Initialize the listview
     */
    private void initList() {

//        adapter = new SimpleCursorAdapter(
//            this,
//            android.R.layout.simple_list_item_2,
//            dbHelper.fetchAllTeams(),
//            new String[] { SportDbHelper.COLUMN_TEAM_NAME, SportDbHelper.COLUMN_LEAGUE_NAME },
//            new int[] { android.R.id.text1, android.R.id.text2}
//        );
        adapter = new TeamRecyclerAdapter(this);

        this.listTeam.setAdapter(adapter);
        listTeam.setLayoutManager(new LinearLayoutManager(this));

//        this.listTeam.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                Team team = dbHelper.getAllTeams().get(position);
//
//                Intent myIntent = new Intent(MainActivity.this, TeamActivity.class);
//                myIntent.putExtra(Team.TAG, team);
//                MainActivity.this.startActivity(myIntent);
//            }
//        });

    }

    ItemTouchHelper.SimpleCallback removeCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

            int position = viewHolder.getAdapterPosition();
            long id = MainActivity.DB.getAllTeams().get(position).getId();

            MainActivity.DB.deleteTeam(id);

            adapter.notifyDataSetChanged();
        }
    };

    private final class RefreshTeamContent extends AsyncTask<Team, Void, String> {

        @Override
        protected String doInBackground(Team... params) {

            Team team = params[0];

            // Prepare the team to be updated

            JSONResponseHandlerTeam responseHandlerTeam = new JSONResponseHandlerTeam(team);

            // Update image
            TeamActivity.loadTeamContent(team, responseHandlerTeam, MainActivity.this);

            // Update last event
            TeamActivity.loadTeamLastEvent(team, responseHandlerTeam, MainActivity.this);

            // https://www.thesportsdb.com/api/v1/json/1/lookuptable.php?l=4430&s=1920
            // Update rank
            TeamActivity.loadTeamRank(team, responseHandlerTeam, MainActivity.this);

            // Save It into the DB
            MainActivity.DB.updateTeam(team);

            return TeamActivity.DONE;
        }

        @Override
        protected void onPostExecute(String result) {

            MainActivity.nbrThreadsRunningRefresh--;

            // If It's the last thread to finish It job
            if (MainActivity.nbrThreadsRunningRefresh <= 0) {

                // Update the listview
                MainActivity.adapter.notifyDataSetChanged();

                MainActivity.swipe.setRefreshing(false);

                Log.d("RefreshTeamContent","Finished");
            }
        }
    }
}
